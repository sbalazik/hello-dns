#pragma once

#include <bits/stdc++.h>

#include "sclasses.hh"
#include "record-types.hh"

using namespace std;

typedef pair<DNSName, ComboAddress> server_t;

timeval now();
const timeval to_timeval(std::chrono::system_clock::time_point tp);

class SelectionAlgorithm {
    public:
        virtual const server_t choose_from(DNSName zone, const multimap<DNSName, ComboAddress>& mservers) = 0;
        virtual void rtt(DNSName zone, server_t, int rtt) = 0;
        virtual void timeout(DNSName zone, server_t) = 0;
};

class RandomSelection: public SelectionAlgorithm {
    public:
        const server_t choose_from(DNSName zone, const multimap<DNSName, ComboAddress>& mservers) {
            vector<server_t> servers;
            for(auto& sp : mservers)
                servers.push_back(sp);
            std::random_device rd;
            mt19937 g(rd());
            std::shuffle(servers.begin(), servers.end(), g);
            return servers.front();
        }

        void rtt(DNSName zone, server_t s, int rtt) {
            cout << rtt << endl;
            return;
        }

        void timeout(DNSName zone, server_t s) {
            return;
        }
};

// Stolen PowerDNS code begins here

void normalizeTV(struct timeval& tv);
const struct timeval operator+(const struct timeval& lhs, const struct timeval& rhs);
const struct timeval operator-(const struct timeval& lhs, const struct timeval& rhs);
const bool operator==(const struct timeval& lhs, const struct timeval& rhs);
const bool operator!=(const struct timeval& lhs, const struct timeval& rhs);


inline float makeFloat(const struct timeval& tv)
{
  return tv.tv_sec + tv.tv_usec/1000000.0f;
}

class DecayingEwma
{
public:
  DecayingEwma() :  d_val(0.0)
  {
    d_needinit=true;
    d_last.tv_sec = d_last.tv_usec = 0;
    d_lastget=d_last;
  }

  DecayingEwma(const DecayingEwma& orig) : d_last(orig.d_last),  d_lastget(orig.d_lastget), d_val(orig.d_val), d_needinit(orig.d_needinit)
  {
  }

  void submit(int val, const struct timeval* tv)
  {
    struct timeval now=*tv;

    if(d_needinit) {
      d_last=now;
      d_lastget=now;
      d_needinit=false;
      d_val = val;
    }
    else {
      float diff= makeFloat(d_last - now);
      d_last=now;
      double factor=exp(diff)/2.0; // might be '0.5', or 0.0001
      d_val=(float)((1-factor)*val+ (float)factor*d_val);
    }
  }

  double get(const struct timeval* tv)
  {
    struct timeval now=*tv;
    float diff=makeFloat(d_lastget-now);
    d_lastget=now;
    float factor=exp(diff/60.0f); // is 1.0 or less
    return d_val*=factor;
  }

  double peek(void) const
  {
    return d_val;
  }

  bool stale(time_t limit) const
  {
    return limit > d_lastget.tv_sec;
  }

private:
  struct timeval d_last;          // stores time
  struct timeval d_lastget;       // stores time
  float d_val;
  bool d_needinit;
};

// End of stolen PowerDNS code

class DecayingEwmaSelection: public SelectionAlgorithm {
    public:
        const server_t choose_from(DNSName zone, const multimap<DNSName, ComboAddress>& mservers) {
            vector<pair<server_t, float>> speeds;
            for (auto& server: mservers) {
                auto n = now();
                auto speed = nsspeeds[zone][server].get(&n);
                speeds.push_back(make_pair(server, speed));
                cout << "Choice" << endl;
                cout << server.first << "\t" << speed << endl;
            }

            // This is basically SyncRes::shuffleInSpeedOrder
            std::random_device rd;
            mt19937 g(rd());
            std::shuffle(speeds.begin(), speeds.end(), g);

            std::stable_sort(speeds.begin(), speeds.end(), [](auto &left, auto &right) {
                return left.second < right.second;
            });

            cout << "Selected: " << speeds.front().first.first << "\t" << speeds.front().second << endl << endl;
            return speeds.front().first;
        }

        void rtt(DNSName zone, server_t s, int rtt) {
            cout << "Updating " << s.first << " " << rtt << endl;
            auto n = now();
            nsspeeds[zone][s].submit(rtt, &n);
        }

        void timeout(DNSName zone, server_t s) {
            rtt(zone, s, 1000000);
        }

    private:
        map<DNSName, map<server_t, DecayingEwma>> nsspeeds;
};

/* Adopted from BIND */

#define DEFAULT		7	/*%< default scale */
#define REPLACE		0	/*%< replace with our rtt */
#define AGE		    10	/*%< age this rtt */

class SRTTSelection: public SelectionAlgorithm {
     public:
        const server_t choose_from(DNSName zone, const multimap<DNSName, ComboAddress>& mservers) {
            auto n = now();
            vector<pair<server_t, unsigned int>> speeds;
            for (auto& server: mservers) {
                if (!srtts[server.second].inicialized) {
                    std::random_device dev;
                    std::mt19937 rng(dev());
                    std::uniform_int_distribution<std::mt19937::result_type> dist(1,32);
                    srtts[server.second] = entry{n, dist(rng), zone};
                    cout << "inicialized " << server.first << " with " << srtts[server.second].srtt << endl;
                    srtts[server.second].inicialized = true;
                }

                auto e = srtts[server.second];
                if (e.zone == zone) {
                    speeds.push_back(make_pair(server, e.srtt));
                    cout << "Choice " << server.first << "\t" << e.srtt << endl;
                }
            }

            std::stable_sort(speeds.begin(), speeds.end(), [](auto &left, auto &right) {
                return left.second < right.second;
            });

            cout << "Picked " << speeds.front().first.first << endl;
            return speeds.front().first;
        }

        void rtt(DNSName zone, server_t s, int rtt) {
            for (auto& e : srtts) {
                if (e.first == s.second) {
                    adjustsrtt(&(e.second), rtt, DEFAULT, now());
                    cout << "Updating " << s.first << " " << rtt << " " << e.second.srtt << endl;
                } else {
                    adjustsrtt(&(e.second), rtt, AGE, now());
                }
            }
        }

        void timeout(DNSName zone, server_t s) {
            uint32_t value, mask;
            std::random_device rd;
            std::mt19937_64 gen(rd());
            std::uniform_int_distribution<uint32_t> dis;
            value = dis(gen);
			if (srtts[s.second].srtt > 800000)
				mask = 0x3fff;
			else if (srtts[s.second].srtt > 400000)
				mask = 0x7fff;
			else if (srtts[s.second].srtt > 200000)
				mask = 0xffff;
			else if (srtts[s.second].srtt > 100000)
				mask = 0x1ffff;
			else if (srtts[s.second].srtt > 50000)
				mask = 0x3ffff;
			else if (srtts[s.second].srtt > 25000)
				mask = 0x7ffff;
			else
				mask = 0xfffff;

            srtts[s.second].srtt += (value & mask);
        }
    private:
        struct entry {
            timeval lastage;
            uint64_t srtt;
            DNSName zone;
            bool inicialized = false;
        };
        static void adjustsrtt(entry *entry, unsigned int rtt, unsigned int factor, timeval now)
        {
            uint64_t new_srtt;

            if (factor == AGE) {
                if (entry->lastage != now) {
                    new_srtt = entry->srtt;
                    new_srtt <<= 9;
                    new_srtt -= entry->srtt;
                    new_srtt >>= 9;
                    entry->lastage = now;
                } else
                    new_srtt = entry->srtt;
            } else
                new_srtt = ((uint64_t)entry->srtt / 10 * factor)
                    + ((uint64_t)rtt / 10 * (10 - factor));

            entry->srtt = (unsigned int) new_srtt;
        }

        map<ComboAddress, entry> srtts;
};


class UnboundNormalSelection: public SelectionAlgorithm {
    public:
        const server_t choose_from(DNSName zone, const multimap<DNSName, ComboAddress>& mservers) {
            vector<server_t> servers;
            for (auto& server: mservers) {
                unsigned int srtt = srtts[server.second].srtt;
                if (srtt <= 400)
                    servers.push_back(server);

                std::random_device rd;
                mt19937 g(rd());
                std::shuffle(servers.begin(), servers.end(), g);

                return servers.front();
            }
        }

        void rtt(DNSName zone, server_t s, int rtt) {
            rtt /= 1000; // Unbound counts in miliseconds
            int delta = rtt - srtts[s.second].srtt;
	        srtts[s.second].srtt += delta / 8;
        }

        void timeout(DNSName zone, server_t s) {
            return;
        }

    private:
        struct entry {
            unsigned int srtt = 376;
        };
        map<ComboAddress, entry> srtts;
};


// Screw Unbound, since it is too complicated (and intertwined with the whole iterator code),
// messing with timeouts and stuff.
// Knot Resolver's algorithm is not worth rewriting (both bad and complicated).
// Let's use PowerDNS' and BIND's algorithms as benchmarks for these new ones.

// Epsilon-greedy with static decay factor and SRRT



class EpsilonGreedy: public SelectionAlgorithm {
    public:
        EpsilonGreedy() {
            epsilon = 0.10;
            decay_factor = 511/512;
            update_factor = 0.3;
        }

        EpsilonGreedy(float epsilon, float decay_factor, float update_factor) : epsilon(epsilon), decay_factor(decay_factor), update_factor(update_factor)
        {}

        const server_t choose_from(DNSName zone, const multimap<DNSName, ComboAddress>& mservers) {
            vector<pair<server_t,float>> rtts;
            float cur_band = band;
            while (rtts.empty()) {
                for (auto& server: mservers) {
                    float rtt = expected_rtt[make_pair(zone, server)].rtt;
                    if (rtt < cur_band) {
                        rtts.push_back(make_pair(server, rtt));
                    }
                }
                cur_band *= 2;
            }

            std::random_device device;
            std::mt19937 gen = std::mt19937(device());

            std::shuffle(rtts.begin(), rtts.end(), gen);

            if(!should_explore(epsilon, gen)) {
                std::stable_sort(rtts.begin(), rtts.end(), [](auto &left, auto &right) {
                    return left.second < right.second;
                });
            }

            return rtts.front().first;
        }

        void rtt(DNSName zone, server_t s, int rtt) {
            for (auto& record: expected_rtt) {
                if(make_pair(zone, s) == record.first) {
                    // Update chosen one
                    record.second.rtt = (1-update_factor) * record.second.rtt + update_factor * rtt;
                } else if (zone == record.first.first) {
                    // Decay others
                    record.second.rtt = (1-decay_factor) * record.second.rtt;
                }
            }
        }

        void timeout(DNSName zone, server_t s) {
            uint32_t value, mask;
            std::random_device rd;
            std::mt19937_64 gen(rd());
            std::uniform_int_distribution<uint32_t> dis;
            auto index = make_pair(zone, s);
            value = dis(gen);
			if (expected_rtt[index].rtt > 800000)
				mask = 0x3fff;
			else if (expected_rtt[index].rtt > 400000)
				mask = 0x7fff;
			else if (expected_rtt[index].rtt > 200000)
				mask = 0xffff;
			else if (expected_rtt[index].rtt > 100000)
				mask = 0x1ffff;
			else if (expected_rtt[index].rtt > 50000)
				mask = 0x3ffff;
			else if (expected_rtt[index].rtt > 25000)
				mask = 0x7ffff;
			else
				mask = 0xfffff;

            expected_rtt[index].rtt += (value & mask);
        }

    private:
        float epsilon;
        float decay_factor;
        float update_factor;
        float band = 400000;

        struct srrt {
            float rtt = 0;
        };

        map<pair<DNSName, server_t>, srrt> expected_rtt;


        bool should_explore(float epsilon, std::mt19937 gen) {
            std::uniform_real_distribution<> dis = std::uniform_real_distribution<>(0.0, 1.0);
            return dis(gen) < epsilon;
        }

};

class SlidingWindowUCB: public SelectionAlgorithm {
    public:
        SlidingWindowUCB(unsigned int window_size) : window_size(window_size)
        {}

        SlidingWindowUCB() {
            window_size = 500;
        }

        const server_t choose_from(DNSName zone, const multimap<DNSName, ComboAddress>& mservers) {
            vector<server_t> servers;
            for (auto& server: mservers)
                servers.push_back(server);
            return get_current_best(zone, servers);
        }
        void rtt(DNSName zone, server_t s, int rtt) {
            add_to_window(zone, s, (float)rtt);
        }
        void timeout(DNSName zone, server_t s) {
            add_to_window(zone, s, 1000000.0);
        }


    private:
        unsigned int window_size;
        float ucb_factor = 100000;

        struct entry {
            server_t server;
            float exp;
        };

        struct window {
            unsigned int t = 0;
            entry* entries = NULL;
        };

        map<DNSName, window> storage;

        void add_to_window(DNSName z, server_t s, float r) {
            // cout << "Adding " << r << " for " << s.first << " in zone " << z << endl;
            window* w = &storage[z];
            if (!w->entries) {
                w->entries = new entry[window_size];
            }
            entry e = {s, r};
            w->entries[w->t % window_size] = e;
            w->t++;
        }

        float expected_rtt(DNSName z, server_t s) {
            // cout << "Counting expected for " << s.first << " in zone " << z << endl;
            window w = storage[z];
            unsigned int c = min(window_size, w.t);

            // cout << "From " << c << " samples:" << endl;

            float sum = 0;
            int count = 0;

            for (unsigned int i = 0; i < c; i++) {
                if (w.entries[i].server == s) {
                    count++;
                    // cout << w.entries[i].exp << endl;
                    sum += w.entries[i].exp;
                }
            }

            if (count == 0)
                return 0;

            return sum/count;
        }

        float confidence_bound(DNSName z, server_t s) {
            window w = storage[z];
            unsigned int c = min(window_size, w.t);

            int count = 0;
            // cout << s.first << endl;
            for (unsigned int i = 0; i < c; i++) {
                if (w.entries[i].server == s) {
                    // cout << w.entries[i].exp << endl;
                    count++;
                }
            }
            float ertt = expected_rtt(z,s);

            float margin = ucb_factor * (count ? sqrt(log(c)/count) : 1);
            cout << "Expected: " << ertt << " -" << margin << " for " << s.first << endl;
            return ertt - margin;
        }

        server_t get_current_best(DNSName z, vector<server_t> servers) {
            vector<pair<server_t, float>> ucbs;
            std::random_device rd;
            mt19937 g(rd());
            std::shuffle(servers.begin(), servers.end(), g);

            for (auto& s : servers) {
                ucbs.push_back(make_pair(s, confidence_bound(z,s)));
            }

            std::stable_sort(ucbs.begin(), ucbs.end(), [](auto &left, auto &right) {
                return left.second < right.second;
            });
            cout << "Choosing: " << ucbs.front().first.first << " " << ucbs.front().second << endl;
            return ucbs.front().first;

        }

};

